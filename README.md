# Scientific methodology Project : CO2 concentration in the atmosphere since 1958 

## Introduction

Each month, CO2 athmospheric level is measured in the Mauna Loa observatory, in Hawaii. Data provided here combines measurements since 1958. In the context of this study, I'll take the dataset of 15/01/20 (17h).

## The (accepted) mission
1. Make a plot that shows the superposition of a periodic oscillation and a slower systematic evolution.
2. Separate these two phenomena. Characterize the periodic oscillation. Find a simple model for the slow contribution, estimate its parameters, and attempt an extrapolation until 2025 (for validating the model using future observations).

### About data
The provided data file contains 10 columns.  Columns 1-4 give the dates in several redundant formats. Column 5 below gives monthly Mauna Loa CO2 concentrations in micro-mol CO2 per mole (ppm), reported on the 2008A SIO manometric mole fraction scale. This is the standard version of the data most often sought. The monthly values have been adjusted to 24:00 hours on the 15th of each month.  Column 6 gives the same data after a seasonal adjustment to remove the quasi-regular seasonal cycle. The adjustment involves subtracting from the data a 4-harmonic fit with a linear gain factor.  Column 7 is a smoothed version of the data generated from a stiff cubic spline function plus 4-harmonic functions with linear gain.  Column 8 is the same smoothed version with the seasonal cycle removed.  Column 9 is identical to Column 5 except that the missing values from Column 5 have been filled with values from Column 7.  Column 10 is identical to Column 6 except missing values have been filled with values from Column 8.  Missing values are denoted by -99.99.




